cd $CLFS/sources

case "${CLFS_BRANCH}" in
	development)
		;;
	release)
		if [ ! -f wget-1.10.2.tar.gz ]
		then
			wget http://ftp.gnu.org/gnu/wget/wget-1.10.2.tar.gz
		fi
		;;
	*)
		echo "Branch not selected."
esac

rm -rf wget-1.10.2
tar -zxf wget-1.10.2.tar.gz
cd wget-1.10.2

./configure --prefix=${CLFS_PREFIX} --build=${CLFS_HOST} --host=${CLFS_TARGET} || exit 1
make || exit 1

make install || exit 1

