cd $CLFS/sources

case "${CLFS_BRANCH}" in
	development)
		;;
	release)
		if [ ! -f libffi-3.0.10.tar.gz ]
		then
			wget ftp://sourceware.org/pub/libffi/libffi-3.0.10.tar.gz
		fi
		;;
	*)
		echo "Branch not selected."
esac

rm -rf libffi-3.0.10
tar -zxf libffi-3.0.10.tar.gz
cd libffi-3.0.10

./configure --prefix=${CLFS_PREFIX} --build=${CLFS_HOST} --host=${CLFS_TARGET}
make

make install

